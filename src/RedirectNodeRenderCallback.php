<?php

namespace Drupal\redirect_node;

use Drupal\Core\Render\Element\RenderCallbackInterface;

/**
 * Implements a trusted preRender callback.
 */
class RedirectNodeRenderCallback implements RenderCallbackInterface {

  /**
   * Pre render callback callback to alter urls for redirect nodes in menus.
   *
   * @param array $build
   *   The render array.
   *
   * @See \Drupal\redirect_node\RedirectNodeRenderCallback::preRender().
   *
   * @return array
   *   The altered render array.
   */
  public static function preRender(array $build) {
    // Calls _redirect_node_replace_node_url_walk_array() recursively on all menu
    // items and their children. If the url for a menu item is rewritten, then a js
    // script is attached that will inject edit links on each of the menu items.
    if (isset($build['content']['#items'])) {
      $includeLibrary = _redirect_node_replace_node_url_walk_array($build['content']['#items']);
      if ($includeLibrary) {
        $build['#attached']['library'][] = 'redirect_node/redirect_node.edit_link';
      }
    }
    return $build;
  }

}
